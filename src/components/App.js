import React, { Component } from "react";
import NewGame from './NewGame'
import GamesSummary from './GamesSummary'

import './main.css';

export function generateRandomDate() {
  return new Date(+(new Date()) - Math.floor(Math.random() * 10000000000));
}

export function totalScore() {
  return this.homeScore + this.awayScore;
}

class App extends Component {

  state = {
    playedGames: [
      {
        homeTeam: 'Mexico',
        awayTeam: 'Canada',
        homeScore: 0,
        awayScore: 5,
        datePlayed: generateRandomDate(),
        totalScore,
      },
      {
        homeTeam: 'Spain',
        awayTeam: 'Brazil',
        homeScore: 10,
        awayScore: 2,
        datePlayed: generateRandomDate(),
        totalScore,
      },
      {
        homeTeam: 'Germany',
        awayTeam: 'France',
        homeScore: 2,
        awayScore: 2,
        datePlayed: generateRandomDate(),
        totalScore,
      },
      {
        homeTeam: 'Uruguay',
        awayTeam: 'Italy',
        homeScore: 6,
        awayScore: 6,
        datePlayed: generateRandomDate(),
        totalScore,
      },
      {
        homeTeam: 'Argentina',
        awayTeam: 'Australia',
        homeScore: 3,
        awayScore: 1,
        datePlayed: generateRandomDate(),
        totalScore,
      },
    ],
    currentGame: {
      homeTeam: '',
      awayTeam: '',
      homeScore: 0,
      awayScore: 0,
    }
  }

  startNewMatch = ({ target }) => {
    this.setState((prevState) => ({
      currentGame: {
        ...prevState.currentGame,
        [target.name]: target.value
      }
    }))
  }

  updateScore = (scoreProp) => {
    this.setState((prevState) => ({
      currentGame: {
        ...prevState.currentGame,
        [scoreProp]: prevState.currentGame[scoreProp] + 1,
      }
    }))
  }

  finishMatch = () => {
    this.setState({
      playedGames: [...this.state.playedGames, {
        ...this.state.currentGame,
        datePlayed: new Date(),
        totalScore,
      }],
      currentGame: {
        homeTeam: '',
        awayTeam: '',
        homeScore: 0,
        awayScore: 0,
      }
    })
  }


  render() {
    return (
      <div className="app-outer-container">
        <h1>Football World Cup Score Board by Optima Gaming</h1>
        <div className="app-inner-container">
          <NewGame
            currentGame={this.state.currentGame}
            startNewMatch={this.startNewMatch}
            updateScore={this.updateScore}
            finishMatch={this.finishMatch}
          />
          <GamesSummary playedGames={this.state.playedGames} />

        </div>
      </div>
    )
  }
}

export default App;