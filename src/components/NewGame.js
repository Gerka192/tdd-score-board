import React, { Component } from "react";

import './main.css';

class NewGame extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showGameStart: true,
      inputError: {
        error: false,
        message: 'Please enter a missing team!',
      }
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.finishMatch = this.finishMatch.bind(this);
    this.handleInputError = this.handleInputError.bind(this);
  }

  handleInputError({ homeTeam, awayTeam }) {
    const isError = !(homeTeam && awayTeam);

    this.setState({
      inputError: {
        ...this.state.inputError,
        error: isError,
      },
    })

    return isError;
  }



  handleSubmit(e) {
    e.preventDefault()

    const isInputErrors = this.handleInputError(this.props.currentGame)

    if (isInputErrors) return;

    this.setState({
      showGameStart: false,
    })
  }

  finishMatch() {
    this.props.finishMatch()
    this.setState({
      showGameStart: true,
    })
  }

  errorShowStyle = {
    visibility: 'visible',
    color: 'red',
    textAlign: 'center'
  }

  errorHideStyle = {
    visibility: 'hidden'
  }

  render() {
    return (
      this.state.showGameStart ?
        <div>
          <p style={this.state.inputError.error ? this.errorShowStyle : this.errorHideStyle}>{this.state.inputError.message}</p>
          <form className="new-game-form">
            <label data-home-label="homeLabel">Home Team</label>
            <input
              type="text"
              name="homeTeam"
              data-home-input="homeInput"
              placeholder="Please enter home team"
              onChange={this.props.startNewMatch}
            />
            <label data-away-label="awayLabel">Away Team</label>
            <input
              type="text"
              name="awayTeam"
              data-away-input="awayInput"
              placeholder="Please enter away team"
              onChange={this.props.startNewMatch}
            />
            <button type="submit" onClick={this.handleSubmit}>Start New Game</button>
          </form>

        </div>
        :
        <div className="new-game-ongoing-container">
          <h2>CURRENT GAME</h2>
          <div className="new-game-ongoing-home">
            <p data-home-ongoing="homeOngoing">
              HOME TEAM: {this.props.currentGame.homeTeam} - {this.props.currentGame.homeScore}
            </p>
            <button
              data-game-home-update="updateHome"
              onClick={() => this.props.updateScore('homeScore')}
            >
              {this.props.currentGame.homeTeam} scored +1
          </button>
          </div>
          <div className="new-game-ongoing-away">
            <p data-away-ongoing="awayOngoing">
              AWAY TEAM: {this.props.currentGame.awayTeam} - {this.props.currentGame.awayScore}
            </p>
            <button
              data-game-away-update="updateAway"
              onClick={() => this.props.updateScore('awayScore')}
            >
              {this.props.currentGame.awayTeam} scored +1
           </button>
          </div>
          <button type="button" data-game-finsih="finishGame" onClick={this.finishMatch}>FINISH GAME</button>
        </div>
    )
  }
}

export default NewGame;