import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from './App';
import NewGame from './NewGame'
import GamesSummary from './GamesSummary'

configure({ adapter: new Adapter() })

describe('App Component', () => {

  let appWrapper;

  beforeAll(() => {
    appWrapper = shallow(<App />);
  })

  it('Should render correct title', () => {
    const { getByText } = render(<App />);
    const appTitleElement = getByText('Football World Cup Score Board by Optima Gaming');
    expect(appTitleElement).toBeInTheDocument();
  })

  it('Should have initial state with 5 games played', () => {
    const STATE_PROP = 'playedGames';
    const wrapperState = appWrapper.state(STATE_PROP);

    expect(appWrapper.state()).not.toBeNull();
    expect(appWrapper.state()).toHaveProperty(STATE_PROP);
    expect(wrapperState).toHaveLength(5);
    expect(wrapperState[0]).toMatchObject({
      homeTeam: expect.any(String),
      awayTeam: expect.any(String),
      homeScore: expect.any(Number),
      awayScore: expect.any(Number),
      datePlayed: expect.any(Date),
      totalScore: expect.any(Function),
    });

    expect(wrapperState[0].totalScore()).toEqual(wrapperState[0].homeScore + wrapperState[0].awayScore)
  })

  it('Should have current game state', () => {
    const CURR_GAME_PROP = 'currentGame'
    expect(appWrapper.state()).toHaveProperty(CURR_GAME_PROP);
    expect(appWrapper.state(CURR_GAME_PROP)).toMatchObject({
      homeTeam: '',
      awayTeam: '',
      homeScore: 0,
      awayScore: 0,
    })
  })

  it('Should render NewGame and GamesSummary components', () => {

    expect(appWrapper.containsMatchingElement(<NewGame />)).toEqual(true);
    expect(appWrapper.containsMatchingElement(<GamesSummary />)).toEqual(true);

  })

  it('Should pass playedGames state property to GamesSummary as prop', () => {
    const gamesSummaryWrapper = appWrapper.find(GamesSummary)
    expect(gamesSummaryWrapper.props().playedGames).toEqual(appWrapper.state().playedGames);
  })

  it('Should be handling start of new game', () => {
    expect(appWrapper.instance().startNewMatch).toBeTruthy();
  })

  it('Should be handling update of an ongoing game', () => {
    expect(appWrapper.instance().updateScore).toBeTruthy();
  })

  it('Should finish current game and update playedGames state with new game', () => {
    expect(appWrapper.instance().finishMatch).toBeTruthy();
  })
})


