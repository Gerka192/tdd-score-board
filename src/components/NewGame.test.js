import React from 'react';
import '@testing-library/jest-dom';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NewGame from './NewGame'

configure({ adapter: new Adapter() })

describe('NewGame Component', () => {

  let newGameWrapper;

  const updateScoreMock = jest.fn();

  const props = {
    currentGame: {
      homeTeam: 'Japan',
      awayTeam: 'China',
      homeScore: 0,
      awayScore: 2
    },
    updateScore: updateScoreMock
  }

  const { homeTeam, awayTeam, homeScore, awayScore } = props.currentGame;

  beforeAll(() => {

    newGameWrapper = shallow(<NewGame {...props} />)
  })

  it('Should have 2 input fields for home and away teams with appropriate labels', () => {

    const homeTeamInput = newGameWrapper.find('[data-home-input="homeInput"]');
    const awayTeamInput = newGameWrapper.find('[data-away-input="awayInput"]');
    const homeTeamLabel = newGameWrapper.find('[data-home-label="homeLabel"]');
    const awayTeamLabel = newGameWrapper.find('[data-away-label="awayLabel"]');

    expect(homeTeamLabel.text()).toEqual('Home Team');
    expect(awayTeamLabel.text()).toEqual('Away Team');
    expect(homeTeamInput).toHaveLength(1);
    expect(awayTeamInput).toHaveLength(1);
  })

  it('Should show current game with correct teams passed as props', () => {

    newGameWrapper.setState({ showGameStart: false });

    const homeTeamOngoing = newGameWrapper.find('[data-home-ongoing="homeOngoing"]');
    const awayTeamOngoing = newGameWrapper.find('[data-away-ongoing="awayOngoing"]');

    expect(homeTeamOngoing.text()).toEqual(`HOME TEAM: ${homeTeam} - ${homeScore}`);
    expect(awayTeamOngoing.text()).toEqual(`AWAY TEAM: ${awayTeam} - ${awayScore}`);
  })

  it('Should have button to finish game', () => {

    newGameWrapper.setState({ showGameStart: false });

    const finishGameBtn = newGameWrapper.find('[data-game-finsih="finishGame"]');

    expect(finishGameBtn).toHaveLength(1);
  })

  it('Should have buttons to update the score for each team', () => {

    newGameWrapper.setState({ showGameStart: false });

    const updateHomeBtn = newGameWrapper.find('[data-game-home-update="updateHome"]');
    const updateAwayBtn = newGameWrapper.find('[data-game-away-update="updateAway"]');

    updateHomeBtn.simulate('click')
    updateAwayBtn.simulate('click')

    expect(updateScoreMock).toHaveBeenCalledTimes(2);

  })
})