import React from "react";

export default ({ playedGames = [] }) => {

  const sortedGames = playedGames.slice();

  if (sortedGames.length) {
    sortedGames.sort((a, b) => {
      if (b.totalScore() === a.totalScore()) {
        return new Date(b.datePlayed) - new Date(a.datePlayed);
      } else {
        return b.totalScore() - a.totalScore();
      }
    })
  }

  return (
    sortedGames.length
      ?
      <div className="games-summary-container">
        <h2>PLAYED MATCHES</h2>
        <ul>
          {sortedGames.map(({ homeTeam, homeScore, awayTeam, awayScore }, i) => {
            return (
              <li key={i}>
                <div className="games-summary-item">
                  <section>
                    <p data-home-title="homeTeam">Home Team</p>
                    <p data-home-name={homeTeam}>{homeTeam}</p>
                    <p data-home-score={`score${homeTeam}`}>Score: {homeScore}</p>
                  </section>
                  <section>
                    <p data-away-title="awayTeam">Away Team</p>
                    <p data-away-name={awayTeam}>{awayTeam}</p>
                    <p data-away-score={`score${awayTeam}`}>Score: {awayScore}</p>
                  </section>
                </div>
              </li>
            )
          })}
        </ul>

      </div>
      :
      <p>There are no games that were played. Please check later</p>
  )
}

