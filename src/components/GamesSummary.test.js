import React from 'react';
import '@testing-library/jest-dom';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { totalScore } from './App.js'

import GamesSummary from './GamesSummary'

configure({ adapter: new Adapter() })

describe('GamesSummary Component', () => {

  it('Should render 0 li elements and 0 p tag when there are no games passed', () => {
    const playedGames = [];
    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)
    const gamesSummaryP = gamesSummaryWrapper.find('p');
    const gamesSummaryLi = gamesSummaryWrapper.find('li');

    expect(gamesSummaryP.text()).toEqual('There are no games that were played. Please check later');
    expect(gamesSummaryLi).toHaveLength(0);
  })



  it('Should render 1 li element when 1 game is played', () => {
    const playedGames = [1];
    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)
    const gamesSummaryLis = gamesSummaryWrapper.find('li');

    expect(gamesSummaryLis).toHaveLength(1);
  })

  it('Should render 2 li element when 2 games are played', () => {
    const playedGames = [
      {
        homeTeam: 'Lithuania',
        awayTeam: 'France',
        homeScore: 3,
        awayScore: 1,
        datePlayed: new Date(),
        totalScore
      },
      {
        homeTeam: 'England',
        awayTeam: 'Spain',
        homeScore: 4,
        awayScore: 7,
        datePlayed: new Date(),
        totalScore
      }
    ];
    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)
    const gamesSummaryLis = gamesSummaryWrapper.find('li');

    expect(gamesSummaryLis).toHaveLength(2);
  })

  it('Should render p tags with home team and away team titles', () => {
    const playedGames = [1];

    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)
    const homeTitleP = gamesSummaryWrapper.find('[data-home-title="homeTeam"]');
    const awayTitleP = gamesSummaryWrapper.find('[data-away-title="awayTeam"]');

    expect(homeTitleP.text()).toEqual('Home Team');
    expect(awayTitleP.text()).toEqual('Away Team');
  })


  it('Should render p tags with correct team names and scores', () => {
    const playedGames = [
      {
        homeTeam: 'Lithuania',
        awayTeam: 'France',
        homeScore: 3,
        awayScore: 1,
        datePlayed: new Date(),
        totalScore
      },
      {
        homeTeam: 'England',
        awayTeam: 'Spain',
        homeScore: 4,
        awayScore: 7,
        datePlayed: new Date(),
        totalScore
      },
    ];
    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)

    const { homeTeam, awayTeam, homeScore, awayScore } = playedGames[1]

    const homeNameP = gamesSummaryWrapper.find(`[data-home-name="${homeTeam}"]`);
    const awayNameP = gamesSummaryWrapper.find(`[data-away-name="${awayTeam}"]`);
    const homeScoreP = gamesSummaryWrapper.find(`[data-home-score="score${homeTeam}"]`);
    const awayScoreP = gamesSummaryWrapper.find(`[data-away-score="score${awayTeam}"]`);

    expect(homeNameP.text()).toEqual(homeTeam);
    expect(awayNameP.text()).toEqual(awayTeam);
    expect(homeScoreP.text()).toEqual(`Score: ${homeScore}`);
    expect(awayScoreP.text()).toEqual(`Score: ${awayScore}`);
  })


  it('Should render sorted games by total score. Same total score sorted by most recent date', () => {
    const playedGames = [
      {
        homeTeam: 'Lithuania',
        awayTeam: 'France',
        homeScore: 3,
        awayScore: 1,
        datePlayed: new Date(),
        totalScore
      },
      {
        homeTeam: 'England',
        awayTeam: 'Spain',
        homeScore: 4,
        awayScore: 7,
        datePlayed: new Date(2021, 3, 25),
        totalScore
      },
      {
        homeTeam: 'Poland',
        awayTeam: 'Portugal',
        homeScore: 7,
        awayScore: 4,
        datePlayed: new Date(2021, 3, 10),
        totalScore
      },
    ];
    const gamesSummaryWrapper = shallow(<GamesSummary playedGames={playedGames} />)

    // England VS Spain should be on the top
    const { homeTeam, awayTeam } = playedGames[1];

    const playedGamesLis = gamesSummaryWrapper.find('li');

    const sortedFirstHomeP = playedGamesLis.first().find(`[data-home-name="${homeTeam}"]`);
    const sortedFirstAwayP = playedGamesLis.first().find(`[data-away-name="${awayTeam}"]`);

    expect(sortedFirstHomeP.text()).toEqual(homeTeam);
    expect(sortedFirstAwayP.text()).toEqual(awayTeam);
  })

})