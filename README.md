
## Score board using TDD approach by Germanas Miliuta

For the past year and a half I was working with Vue/Nuxt and writing only E2E tests with Cypress, therefore it was deffinitely a challenge to use React with TDD approach in the past 3 days.

I was trying to commit as often as possible to showcase progress of development by writing tests first as you requested. 

There is minimu style applied, to be able run this project on mobiles and desktops.

To implement functionality of the app is pretty easy, however since I have never done TDD, it took much longer to undersand and switch mentally. 

I would really appreciate your review for this project, so I could improve my TDD skills further.

Many thanks in advance! :) 
